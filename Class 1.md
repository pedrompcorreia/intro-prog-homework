# Class 1

```py

# Exercise 1

Stock_Name = 'APPL'

Stock_Value = 2.5

Trade_Shares = 50

Action = 'Buy'

print('I would like to', Action, Trade_Shares, 'shares in', Stock_Name)


# Exercise 2

Person_1 = 'Madalena'

Euro_Amount = 50

Person_2 = 'Pedro'

Stock_Amount = 25

Company_Name = 'Tesla'

print (Person_1, 'needs to borrow', Euro_Amount, 'euros from', Person_2, 'to trade', Stock_Amount, 'in', Company_Name)


# Exercise 3

Variable_1 = 12

Variable_2 = 24

print (Variable_1*Variable_2)

Multiplication = Variable_1*Variable_2

print(Multiplication)


# Exercise 4

Celsius = 30

Kelvin = Celsius + 273.15

Fahrenheit = Celsius * 1.8 + 32

print (Kelvin)

print (Fahrenheit)

```